function varargout = FEMMES(varargin)
% FEMMES MATLAB code for FEMMES.fig
%      FEMMES, by itself, creates a new FEMMES or raises the existing
%      singleton*.
%
%      H = FEMMES returns the handle to a new FEMMES or the handle to
%      the existing singleton*.
%
%      FEMMES('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in FEMMES.M with the given input arguments.
%
%      FEMMES('Property','Value',...) creates a new FEMMES or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before FEMMES_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All FEMMES are passed to FEMMES_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help FEMMES

% Last Modified by GUIDE v2.5 21-Jan-2018 17:33:33

% Begin initialization code - DO NOT EDIT
% set(handles.figure1, 'Name', 'FEMMES for Finite Time Observers');
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @FEMMES_OpeningFcn, ...
                   'gui_OutputFcn',  @FEMMES_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --- Executes just before FEMMES is made visible.
function FEMMES_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to FEMMES (see VARARGIN)

% Choose default command line output for FEMMES
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

ah = axes('unit','normalized','position',[0.8 0.8 0.2 0.2]);
bg = imread('InriaLogo.jpg');imagesc(bg);
set(ah,'handlevisibility','off','visible','off');

ah = axes('unit','normalized','position',[0 0.8 0.2 0.2]);
bg = imread('InriaLogo.jpg');imagesc(bg);
set(ah,'handlevisibility','off','visible','off');

ah = axes('unit','normalized','position',[0.8 0 0.2 0.2]);
bg = imread('InriaLogo.jpg');imagesc(bg);
set(ah,'handlevisibility','off','visible','off');

ah = axes('unit','normalized','position',[0 0 0.2 0.2]);
bg = imread('InriaLogo.jpg');imagesc(bg);
set(ah,'handlevisibility','off','visible','off');
% UIWAIT makes FEMMES wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = FEMMES_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function TextBox2_Callback(hObject, eventdata, handles)
% hObject    handle to TextBox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TextBox2 as text
%        str2double(get(hObject,'String')) returns contents of TextBox2 as a double


% --- Executes during object creation, after setting all properties.
function TextBox2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TextBox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function TextBox3_Callback(hObject, eventdata, handles)
% hObject    handle to TextBox3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TextBox3 as text
%        str2double(get(hObject,'String')) returns contents of TextBox3 as a double


% --- Executes during object creation, after setting all properties.
function TextBox3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TextBox3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
display('Goodbye');
close(gcf);


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Main_Code;

% H = uicontrol(FigA,'Style','slider','Min',0,'Max',P,'Value',P/2,...
%         'Position', [400 20 120 20]);
% addlistener(H, 'Value', 'PostSet',@myCallBack);
% 
% 
% function myCallBack(hObj,event)
% val = get(event.AffectedObject,'Value')

% function HelloCallback(hObject,event)
% % msgbox('Hello world');
% Main_Code;

% tableData_C = get(handles.uitableC, 'data');



function BoxP_Callback(hObject, eventdata, handles)
% hObject    handle to BoxP (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global P
P = str2double(get(hObject,'String'));
% Hints: get(hObject,'String') returns contents of BoxP as text
%        str2double(get(hObject,'String')) returns contents of BoxP as a double


% --- Executes during object creation, after setting all properties.
function BoxP_CreateFcn(hObject, eventdata, handles)
% hObject    handle to BoxP (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



% --- Executes on slider movement.
function slider1_Callback(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global P theta
% format short
set(handles.BoxTheta, 'String', get(hObject,'Value'));
set(handles.slider1,'Min',0,'Max',P);
theta =get(hObject,'Value');
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function slider1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end




function BoxTheta_Callback(hObject, eventdata, handles)
% hObject    handle to BoxTheta (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.BoxTheta, 'enable', 'off')
global theta
format short
set(handles.slider1, 'Value', str2num(get(handles.BoxTheta, 'String')));
theta = str2double(get(hObject,'String'));
% Hints: get(hObject,'String') returns contents of BoxTheta as text
%        str2double(get(hObject,'String')) returns contents of BoxTheta as a double


% --- Executes during object creation, after setting all properties.
function BoxTheta_CreateFcn(hObject, eventdata, handles)
% hObject    handle to BoxTheta (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function slider2_Callback(hObject, eventdata, handles)
% hObject    handle to slider2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% global theta
global theta T
set(handles.BoxT, 'String', get(hObject,'Value'));
set(handles.slider2,'Min',0,'Max',theta/3);
T =get(hObject,'Value');
% get(hObject,'Min',0,'Max',theta/3,'Value',theta/6);
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function slider2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


function BoxT_Callback(hObject, eventdata, handles)
% hObject    handle to BoxT (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.BoxT, 'enable', 'off')
global T
set(handles.slider2, 'Value', str2num(get(handles.BoxT, 'String')));
T = str2double(get(hObject,'String'));
% T = str2double(get(hObject,'String'))
% Hints: get(hObject,'String') returns contents of BoxT as text
%        str2double(get(hObject,'String')) returns contents of BoxT as a double


% --- Executes during object creation, after setting all properties.
function BoxT_CreateFcn(hObject, eventdata, handles)
% hObject    handle to BoxT (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function BoxTau_Callback(hObject, eventdata, handles)
% hObject    handle to BoxTau (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global tau
tau = str2double(get(hObject,'String'));
% Hints: get(hObject,'String') returns contents of BoxTau as text
%        str2double(get(hObject,'String')) returns contents of BoxTau as a double


% --- Executes during object creation, after setting all properties.
function BoxTau_CreateFcn(hObject, eventdata, handles)
% hObject    handle to BoxTau (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

      
%       function pushbutton2_Callback(hObject, eventdata, handles) 
% % hObject handle to pushbutton2 (see GCBO)
% % eventdata reserved - to be defined in a future version of MATLAB 
% % handles structure with handles and user data (see GUIDATA)
% Main_Code; %m file to be executed.
% ButtonH=uicontrol('Parent',FigA,'Style','pushbutton','String','View Data','Units','normalized','Position',[0.5 0.5 0.4 0.2],'Visible','on');
% 
% 

function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double


% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit3_Callback(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit3 as text
%        str2double(get(hObject,'String')) returns contents of edit3 as a double


% --- Executes during object creation, after setting all properties.
function edit3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function BoxTSimu_Callback(hObject, eventdata, handles)
% hObject    handle to BoxTSimu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global TSimu
TSimu = str2double(get(hObject,'String'));
% Hints: get(hObject,'String') returns contents of BoxTSimu as text
%        str2double(get(hObject,'String')) returns contents of BoxTSimu as a double


% --- Executes during object creation, after setting all properties.
function BoxTSimu_CreateFcn(hObject, eventdata, handles)
% hObject    handle to BoxTSimu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
