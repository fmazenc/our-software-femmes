%% 
% FEMMES-Finite Time Estimation Meysam Mazenc Et Saeed 
% For more details about FEMMES algorithm please see:
% Mazenc, F., Ahmed, S., & Malisoff, M. (2017). 
% Finite time estimation through a continuous-discrete observer. 
% International Journal of Robust and Nonlinear Control, 2017.
% Last Modified 23 January, 2018 By Saeed Ahmed

%% 
% Input prompt for model and observer parameters and 
% verification of Assumption 1
syms t s
global T tau theta P TSimu
prompt_A = 'Please enter the system matrix A = ';
A = input(prompt_A)
n_st=length(A);
prompt_B = 'Please enter the input matrix B = ';
B = input(prompt_B)
m1=size(B);
n_in=m1(2);
prompt_C = 'Please enter the output matrix C = ';
C = input(prompt_C)
m2=size(C);
n_out=m2(2);

Ob = obsv(A,C);
unob = length(A)-rank(Ob);
if unob~=0
    h = msgbox('A and C are not observable');
    inputs
    close Figure 1
    return 
end

prompt_L = 'Please enter the gain matrix L = ';
L = input(prompt_L)

expA=simplify(ilaplace(inv(s*eye(size(A))-A)));
y1 = expA*B;
y2 = C*expA*B;

%% 
% Verification of Assumption 2
H = A+L*C*subs(expA,-tau);
expH=simplify(ilaplace(inv(s*eye(size(H))-H)));
M_inv=double(det(subs(expA,-T)-subs(expH,-T)));
detM_inv = det(M_inv);
while abs(detM_inv)<0.001
    detM_inv = det(M_inv)
    prompt_L = 'Please choose another L so that the inverse of M exists, L = ';
    L = input(prompt_L)
    H = A+L*C*subs(expA,-tau);
    expH=simplify(ilaplace(inv(s*eye(size(H))-H)));
    M_inv=double(det(subs(expA,-T)-subs(expH,-T)));
    detM_inv = det(M_inv);
end
detM_inv = det(M_inv)
disp('Congratulations! Assumption 2 is satisfied, so please proceed forward...')
M=inv(subs(expA,-T)-subs(expH,-T));

%% 
% Input prompt for initial conditions of state and observer
prompt_state_initial_condition = 'Please enter the initial condition for true state =  ';
x0 = input(prompt_state_initial_condition)
prompt_observer_initial_condition = 'Please enter the initial condition for estimated state = ';
hat_x0 = input(prompt_observer_initial_condition)

%% 
% Simulation Parameters
dt = 0.001;
NbIter = round((TSimu)/dt);
nT = round(T/dt);
t=0:dt:TSimu;

%% 
% Input prompt for input and disturbances
prompt_u = 'Please specify the input u(t) = ';
u(1,:) = input(prompt_u);
prompt_delta_1 = 'Please specify the input disturbance delta_1(t) = ';
delta_1(:,:) = input(prompt_delta_1);
prompt_delta_2 = 'Please specify the measurement noise delta_2(t) = ';
delta_2(:,:) = input(prompt_delta_2);

%% 
% Simulation parameters
h2=1e-1;
NbIter2 = round((T)/h2);
n=round(T/h2)+1;
h3=1e-1;
NbIter3 = round((tau)/h3);
time = zeros(NbIter,1);
q=1;
delta(1)=P+2*theta/3;
amp=0.5;
x10 = 2;
x20 = 2;
vx = zeros(n_st,NbIter);
e = zeros(1,NbIter);
vx(:,1) = x0;
vx1(1) = x10;
vx2(1) = x20;

%% 
% Initialization of estimator vector
vhx = zeros(n_st,NbIter);
vhx(:,1) = hat_x0;
hx10 = 0;
hx20 = 0;

%% 
% Main Simulations
for ii=1:NbIter-1
    time(ii) = dt*(ii-1);
%Euler semi-implicit integration algorithm for the model
       dx=A*vx(:,ii)+B*u(ii)+delta_1(:,ii);
       vx(:,ii+1)=vx(:,ii)+dt*dx;
%The continuous-discrete observer is implemented using Euler semi-implicit
%integration algorithm and trapezoidal approximation of integration
    if time(ii)> delta(1)
        dhx=A*vhx(:,ii)+B*u(ii);
       vhx(:,ii+1)=vhx(:,ii)+dt*dhx;
    end
    if time(ii)-delta(q)>0
        for jj=0:NbIter2
            l=time(ii)-T+jj*h2;
            for jjj=0:NbIter3
                y21(jjj+1)=subs(y2,l-tau-(l-tau+jjj*h3))*u((ii-nT+(1+h2/dt*(jj-1))-tau+jjj));
                x21(jjj+1) = l-tau+jjj*h3;
            end
            y21sub=trapz(x21,y21);
              Y=subs(expH,time(ii)-T-l)*(-B*(u(ii-nT+(1+h2/dt*(jj-1))))+...
               L*(C*vx(:,ii-nT+1+(jj-1)*h2/dt-nT)+delta_2(:,ii-nT+(1+h2/dt*(jj-1))))+L*y21sub);
            y_sub=(subs(y1,time(ii)-T-l))*(u(ii-nT+(1+h2/dt*(jj-1))));
            for iii=1:n_st 
            Y1sub(iii,jj+1)=y_sub(iii,1);
            Y1(iii,jj+1)=Y(iii,1);
            end
            X(jj+1)=l;
        end
        for kk=1:n_st
            I11(kk,1)=trapz(X,Y1(kk,:));
            Ysub(kk,1)=trapz(X,Y1sub(kk,:));
        end
        int_sum = M*(Ysub + I11);
        for k=1:n_st
        vhx(k,ii+1)=double(int_sum(k,1));
        end
        delta(q+1)=(q+1)*P+2*theta/3;
        q=q+1;
    end  
end
time(NbIter) = dt*(NbIter-1);

%% Output Figures of FEMMES
figure,clf,
set(gcf,'name','Output of FEMME-True State and Estimated State');
plot(time,vx(2,:),time,vhx(2,:));
lgnd=legend('True Comp. of State','Est. Comp. of State');
set(lgnd,'Interpreter','latex','Location','best')
xlabel('t(seconds)')
set(gca,'FontSize',15,'FontWeight','normal','linewidth',2)
set(get(gca,'XLabel'),'FontSize',20,'FontWeight','normal')
set(get(gca,'YLabel'),'FontSize',20,'FontWeight','normal')
set(get(gca,'Title'),'FontSize',15,'FontWeight','normal')
clc
